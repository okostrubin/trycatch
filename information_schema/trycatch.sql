DROP DATABASE IF EXISTS `trycatch`;
CREATE DATABASE `trycatch`;
USE `trycatch`;

CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `phone` varchar(20) NOT NULL DEFAULT '',
  `street` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;

INSERT INTO `addresses` (`id`, `name`, `phone`, `street`) VALUES
(1, 'Michal', '506088156', 'Michalowskiego 41'),
(2, 'Marcin', '502145785', 'Opata Rybickiego 1'),
(3, 'Piotr', '504212369', 'Horacego 23'),
(4, 'Albert', '605458547', 'Jan Pawła 67');

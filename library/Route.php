<?php
namespace library;

class Route
{
    public $pattern;
    public $callback;
    public $methods = array();
    public $params = array();

    public function __construct($pattern, $callback, $methods, $pass)
    {
        $this->pattern = $pattern;
        $this->callback = $callback;
        $this->methods = $methods;
    }

    public function matchUrl($url)
    {
        if ($this->pattern === $url)
            return true;
        
        $ids = array();
        $last_char = substr($this->pattern, -1);
        $regex = str_replace(array(')','/*'), array(')?','(/?|/.*?)'), $this->pattern);
        $regex = preg_replace_callback(
            '#@([\w]+)(:([^/\(\)]*))?#',
            function($matches) use (&$ids)
            {
                $ids[$matches[1]] = null;
                if (isset($matches[3]))
                    return '(?P<'.$matches[1].'>'.$matches[3].')';
                return '(?P<'.$matches[1].'>[^/\?]+)';
            },
            $regex
        );
        if ($last_char === '/')
            $regex .= '?';
        else
            $regex .= '/?';
        
        if (preg_match('#^'.$regex.'(?:\?.*)?$#i', $url, $matches))
        {
            foreach ($ids as $k => $v)
                $this->params[$k] = (array_key_exists($k, $matches)) ? urldecode($matches[$k]) : null;
            return true;
        }
        return false;
    }

    public function matchMethod($method)
    {
        return count(array_intersect(array($method, '*'), $this->methods)) > 0;
    }
}

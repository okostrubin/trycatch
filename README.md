# Requirements
PHP Version: PHP 5.5
MySQL

# Installation
Import /information_schema/trycatch.sql
Modify /config/config.ini

# Starter
Usage: php starter.php [option]

Options:
-a1   Sum of all the multiples of 3 or 5 below 1000
-a2   Sum of all the multiples of 3 or 4 below 1000
-b    Power of number 6 to index 5 without using multiplication
-c1   10 numbers for fibonacci series with recursion
-c2   10 numbers for fibonacci series without recursion
-h    Help

# Task Two and Desert
GET       /address/:id   Returns address details by id
POST      /address       Adds new address
 name
 phone
 street
PUT       /address/:id   Updates address details by id
 name
 phone
 street
DELETE    /address/:id   Delete address by id
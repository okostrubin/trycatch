<?php
namespace library;

class Dispatcher
{
    protected $events = array();

    public function run($name, array $params = array())
    {
        $output = '';
        $output = $this->execute($this->get($name), $params);
        return $output;
    }

    public function set($name, $callback)
    {
        $this->events[$name] = $callback;
    }

    public function get($name)
    {
        return isset($this->events[$name]) ? $this->events[$name] : null;
    }

    public static function execute($callback, array &$params = array())
    {
        if (is_callable($callback))
        {
            return is_array($callback) ?
                self::invokeMethod($callback, $params) :
                self::callFunction($callback, $params);
        }
        else
        {
            throw new \Exception('Invalid callback specified.');
        }
    }

    public static function callFunction($func, array &$params = array())
    {
        switch (count($params))
        {
            case 0:
                return $func();
            case 1:
                return $func($params[0]);
            case 2:
                return $func($params[0], $params[1]);
            case 3:
                return $func($params[0], $params[1], $params[2]);
            default:
                return call_user_func_array($func, $params);
        }
    }

    public static function invokeMethod($func, array &$params = array())
    {
        list($class, $method) = $func;
        $instance = is_object($class);
        switch (count($params))
        {
            case 0:
                return ($instance) ?
                    $class->$method() :
                    $class::$method();
            case 1:
                return ($instance) ?
                    $class->$method($params[0]) :
                    $class::$method($params[0]);
            case 2:
                return ($instance) ?
                    $class->$method($params[0], $params[1]) :
                    $class::$method($params[0], $params[1]);
            case 3:
                return ($instance) ?
                    $class->$method($params[0], $params[1], $params[2]) :
                    $class::$method($params[0], $params[1], $params[2]);
            default:
                return call_user_func_array($func, $params);
        }
    }
}

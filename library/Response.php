<?php
namespace library;

class Response
{
    protected $status = 200;
    protected $headers = array();
    protected $body;

    public static $codes = array(
        200 => 'OK',
        400 => 'Bad Request',
        404 => 'Not Found',
        500 => 'Internal Server Error',
    );

    public function status($code = null)
    {
        if ($code === null)
            return $this->status;
        if (array_key_exists($code, self::$codes))
            $this->status = $code;
        else
            throw new \Exception('Invalid status code.');
        return $this;
    }

    public function header($name, $value = null)
    {
        if (is_array($name))
            foreach ($name as $k => $v)
                $this->headers[$k] = $v;
        else
            $this->headers[$name] = $value;
        return $this;
    }

    public function write($str)
    {
        $this->body .= $str;
        return $this;
    }

    public function sendHeaders()
    {
        if (strpos(php_sapi_name(), 'cgi') !== false)
            header(sprintf(
                'Status: %d %s',
                $this->status,
                self::$codes[$this->status]
                ),true);
        else
            header(sprintf(
                '%s %d %s',
                (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.1'),
                $this->status,
                self::$codes[$this->status]),
                true,
                $this->status);

        foreach ($this->headers as $field => $value)
        {
            if (is_array($value))
                foreach ($value as $v)
                    header($field.': '.$v, false);
            else
                header($field.': '.$value);
        }
        return $this;
    }

    public function send()
    {
        if (ob_get_length() > 0)
            ob_end_clean();
        if (!headers_sent())
            $this->sendHeaders();
        exit($this->body);
    }
}
<?php
class Application
{
    private static $framework;
    private function __construct() {}
    private function __destruct() {}
    private function __clone() {}

    public static function __callStatic($name, $params)
    {
        static $initialized = false;
        if (!$initialized)
        {
            require_once __DIR__.'/autoload.php';
            self::$framework = new \library\Framework();
            $initialized = true;
        }
        return \library\Dispatcher::invokeMethod(array(self::$framework, $name), $params);
    }

    public static function app()
    {
        return self::$framework;
    }
}

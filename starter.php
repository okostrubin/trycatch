<?php
class ProjectEulerOne
{
    /**
     * Calculate sum of all the multiples of $divider1 or $divider2 below $below_number
     *
     * @param integer $divider1
     * @param integer $divider2
     * @param integer $below_number
     * @return integer
     */
    public function calculate($divider1, $divider2, $below_number)
    {
        $max_dividend           = $below_number - 1;
        $sum_of_dividers1       = floor($max_dividend / $divider1) * (floor($max_dividend / $divider1) + 1) * $divider1 / 2;
        $sum_of_dividers2       = floor($max_dividend / $divider2) * (floor($max_dividend / $divider2) + 1) * $divider2 / 2;
        $sum_of_dividers1_and_2 = floor($max_dividend / ($divider1 * $divider2)) * (floor($max_dividend / ($divider1 * $divider2)) + 1) * $divider1 * $divider2 / 2;
        $result                 = $sum_of_dividers1 + $sum_of_dividers2 - $sum_of_dividers1_and_2;
        return $result;
    }
}

class Power
{
    public function calculate($base, $exponent)
    {
        $result = 1;
        for ($i = 0; $i < $exponent; $i++)
        {
            $multiply_result = 0;
            for ($j = 0; $j < $base; $j++)
                $multiply_result += $result;
            $result = $multiply_result;
        }
        return $result;
    }
}

class Fibonacci
{
    /**
     * @param integer $start1 first element of sequence
     * @param integer $start2 second element of sequence
     * @param integer $amount amount of elements to calculate
     * @return array
     */
    public function calculate_recursion($start1, $start2, $amount)
    {
        if ($amount != 0)
        {
            $next = $start1 + $start2;
            return [$start1, $this->calculate_recursion($start2, $next, $amount - 1)];
        }
    }
    
    /**
     * @param integer $start1 first element of sequence
     * @param integer $start2 second element of sequence
     * @param integer $amount amount of elements to calculate
     * @return array
     */
    public function calculate($start1, $start2, $amount)
    {
        $result = [$start1, $start2];
        for($i = 1; $i < $amount - 1; $i++)
            $result[] = $result[$i] + $result[$i - 1];
        return $result;
    }
}

interface Task
{
    public function execute();
}

class TaskA1 implements Task
{
    public function execute()
    {
        $taskA1 = new ProjectEulerOne();
        print $taskA1->calculate(3, 5, 1000);
    }
}

class TaskA2 implements Task
{
    public function execute()
    {
        $taskA2 = new ProjectEulerOne();
        print $taskA2->calculate(3, 4, 1000);
    }
}

class TaskB implements Task
{
    public function execute()
    {
        $taskB = new Power();
        print $taskB->calculate(6, 5);
    }
}

class TaskC1 implements Task
{
    public function execute()
    {
        $taskC1 = new Fibonacci();
        foreach ($taskC1->calculate(0, 1, 10) as $value)
            print $value." ";
    }
}

class TaskC2 implements Task
{
    public function execute()
    {
        $taskC2 = new Fibonacci();
        foreach ($taskC2->calculate(0, 1, 10) as $value)
            print $value." ";
    }
}

class Help implements Task
{
    public function execute()
    {
        $help_text =
        "Usage: php starter.php [option]".PHP_EOL.
        PHP_EOL.
        "Options:".PHP_EOL.
        "-a1   Sum of all the multiples of 3 or 5 below 1000".PHP_EOL.
        "-a2   Sum of all the multiples of 3 or 4 below 1000".PHP_EOL.
        "-b    Power of number 6 to index 5 without using multiplication".PHP_EOL.
        "-c1   10 numbers for fibonacci series with recursion".PHP_EOL.
        "-c2   10 numbers for fibonacci series without recursion".PHP_EOL.
        "-h    Shows this message";
        print $help_text;
    }
}

class Starter
{
    private $_task;
    
    public function __construct(Task $task)
    {
        $this->_task = $task;
    }
    
    public function execute()
    {
        $this->_task->execute();
    }
}

if (!empty($argv[1]))
{
    switch ($argv[1]) {
        case '-a1':
            $starter = new Starter(new TaskA1());
            break;
        case '-a2':
            $starter = new Starter(new TaskA2());
            break;
        case '-b':
            $starter = new Starter(new TaskB());
            break;
        case '-c1':
            $starter = new Starter(new TaskC1());
            break;
        case '-c2':
            $starter = new Starter(new TaskC2());
            break;
        case '-h':
        default:
            $starter = new Starter(new Help());
    }
}
else
{
    $starter = new Starter(new Help());
}

$starter->execute();
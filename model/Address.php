<?php
class Address
{
    protected $table = 'addresses';
    private $_db;
    
    public function __construct()
    {
        $config = parse_ini_file( __DIR__ . '/../config/config.ini');
        if ($config === FALSE || is_null($config["host"]) || is_null($config["user"]) || is_null($config["pass"]) || is_null($config["name"]))
            Application::error(new \Exception('Config file error'));
        $mysqli = new mysqli($config["host"], $config["user"], $config["pass"], $config["name"]);
        if (mysqli_connect_errno())
            Application::error(new \Exception('Db configuration error: ' . mysqli_connect_error()));
        else
            $this->_db = $mysqli;
    }
    
    public function __destruct()
    {
        if (!empty($this->_db))
            $this->_db->close();
    }
    
    public function getRecord($id)
    {
        $sql = "SELECT name, phone, street FROM " . $this->table . " WHERE id = " . mysqli_real_escape_string($this->_db, $id);
        $result = $this->_db->query($sql);
        if ($this->_db->affected_rows > 0)
        {
            $data = mysqli_fetch_assoc($result);
            $data['status'] = 'Record was found';
            $data['code'] = 200;
            return $data;
        }
        else
            return ['status' => 'No records found', 'code' => 404];
    }
    
    public function updateRecord($id, $params)
    {
        if (empty($params['name']) || empty($params['phone']) || empty($params['street']))
            return ['status' => 'Bad request', 'code' => 400];
        $name   = mysqli_real_escape_string($this->_db, $params['name']);
        $phone  = mysqli_real_escape_string($this->_db, $params['phone']);
        $street = mysqli_real_escape_string($this->_db, $params['street']);
        $id     = mysqli_real_escape_string($this->_db, $id);
        $sql = "UPDATE " . $this->table . " SET `name` = '".$name."', `phone` = '".$phone."', `street` = '".$street."' WHERE id = " . $id;
        $this->_db->query($sql);
        if ($this->_db->affected_rows > 0)
            return ['status' => 'Record ' . $id . ' was updated', 'code' => 200];
        else
            return ['status' => 'Record was not updated', 'code' => 200];
    }
    
    public function deleteRecord($id)
    {
        $sql = "DELETE FROM " . $this->table . " WHERE id = " . mysqli_real_escape_string($this->_db, $id);
        $this->_db->query($sql);
        if ($this->_db->affected_rows > 0)
            return ['status' => 'Record ' . $id . ' deleted', 'code' => 200];
        else
            return ['status' => 'No records found', 'code' => 200];
    }
    
    public function addRecord($params)
    {
        if (empty($params['name']) || empty($params['phone']) || empty($params['street']))
            return ['status' => 'Bad request', 'code' => 400];
        $name   = mysqli_real_escape_string($params['name']);
        $phone  = mysqli_real_escape_string($params['phone']);
        $street = mysqli_real_escape_string($params['street']);
        $sql = "INSERT INTO " . $this->table . " (`name`, `phone`, `street`) VALUES ('".$name."', '".$phone."', '".$street."')";
        $this->_db->query($sql);
        if ($this->_db->affected_rows > 0)
            return ['status' => 'Record ' . mysqli_insert_id($this->_db) . ' was added', 'code' => 200];
        else
            return ['status' => 'Record was not added', 'code' => 200];
    }
}
<?php
namespace library;

use library\Collection;

class Request
{
    public $url;
    public $base;
    public $method;
    public $query;
    public $data;

    public function __construct($config = array())
    {
        if (empty($config))
        {
            $config = array(
                'url' => self::getVar('REQUEST_URI', '/'),
                'base' => str_replace(array('\\',' '), array('/','%20'), dirname(self::getVar('SCRIPT_NAME'))),
                'method' => self::getMethod(),
                'query' => new Collection($_GET),
                'data' => new Collection($_POST),
            );
        }
        $this->init($config);
    }

    public function init($properties = array())
    {
        foreach ($properties as $name => $value)
            $this->$name = $value;
        if ($this->base != '/' && strlen($this->base) > 0 && strpos($this->url, $this->base) === 0)
            $this->url = substr($this->url, strlen($this->base));
        if (empty($this->url))
        {
            $this->url = '/';
        }
        else
        {
            $_GET += self::parseQuery($this->url);
            $this->query->setData($_GET);
        }
    }

    public static function getMethod()
    {
        if (isset($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE']))
            return $_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'];
        elseif (isset($_REQUEST['_method']))
            return $_REQUEST['_method'];
        return self::getVar('REQUEST_METHOD', 'GET');
    }

    public static function getVar($var, $default = '')
    {
        return isset($_SERVER[$var]) ? $_SERVER[$var] : $default;
    }

    public static function parseQuery($url)
    {
        $params = array();
        $args = parse_url($url);
        if (isset($args['query']))
            parse_str($args['query'], $params);
        return $params;
    }
}

<?php
require 'library/Application.php';

//Controllers
foreach (glob(__DIR__."/controller/*.php") as $filename)
    require_once $filename;

//Models
Application::path("model/");

Application::start();
?>

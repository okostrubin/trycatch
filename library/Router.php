<?php
namespace library;

class Router
{
    protected $routes = array();
    protected $index = 0;

    public function map($pattern, $callback, $pass_route = false)
    {
        $url = $pattern;
        $methods = array('*');
        if (strpos($pattern, ' ') !== false) 
        {
            list($method, $url) = explode(' ', trim($pattern), 2);
            $methods = explode('|', $method);
        }
        $this->routes[] = new Route($url, $callback, $methods, $pass_route);
    }

    public function route(Request $request)
    {
        while ($route = $this->current())
        {
            if ($route->matchUrl($request->url) && $route !== false && $route->matchMethod($request->method))
                return $route;
            $this->next();
        }
        return false;
    }

    public function current()
    {
        if (isset($this->routes[$this->index]))
            return $this->routes[$this->index];
        else
            return false;
    }

    public function next()
    {
        $this->index++;
    }
}

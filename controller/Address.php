<?php
Application::route('GET /address/@id', function($id)
{
    $address = new Address();
    $result = $address->getRecord($id);
    Application::json($result, $result['code']);
});

Application::route('POST /address/', function()
{
    $address = new Address();
    $result = $address->addRecord($_POST);
    Application::json($result, $result['code']);
});

Application::route('PUT /address/@id', function($id)
{
    $_PUT = array();
    $putdata = file_get_contents('php://input');
    $exploded = explode('&', $putdata);
    foreach($exploded as $pair)
    {
        $item = explode('=', $pair);
        if(count($item) == 2)
            $_PUT[urldecode($item[0])] = urldecode($item[1]);
    }
    
    $address = new Address();
    $result = $address->updateRecord($id, $_PUT);
    Application::json($result, $result['code']);
});

Application::route('DELETE /address/@id', function($id)
{
    $address = new Address();
    $result = $address->deleteRecord($id);
    Application::json($result, $result['code']);
});
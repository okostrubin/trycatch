<?php
namespace library;

use library\Loader;
use library\Dispatcher;

class Framework
{
    protected $vars;
    protected $loader;
    protected $dispatcher;

    public function __construct()
    {
        $this->vars = array();
        $this->loader = new Loader();
        $this->dispatcher = new Dispatcher();
        $this->init();
    }

    public function __call($name, $params)
    {
        $callback = $this->dispatcher->get($name);
        if (is_callable($callback))
            return $this->dispatcher->run($name, $params);
        $shared = (!empty($params)) ? (bool)$params[0] : true;
        return $this->loader->load($name, $shared);
    }

    public function init()
    {
        $this->loader->register('request', '\library\Request');
        $this->loader->register('response', '\library\Response');
        $this->loader->register('router', '\library\Router');
        $methods = array(
            'start',
            'route',
            'error',
            'notFound',
        );
        foreach ($methods as $name)
        {
            $this->dispatcher->set($name, array($this, '_'.$name));
        }
    }

    public function start()
    {
        $dispatched = false;
        $request = $this->request();
        $response = $this->response();
        $router = $this->router();
        
        while ($route = $router->route($request))
        {
            $params = array_values($route->params);
            $dispatched = $this->dispatcher->execute(
                $route->callback,
                $params
            );
            $dispatched = true;
            if (!$dispatched) break;
            $router->next();
        }

        if (!$dispatched) {
            $this->notFound();
        }
    }

    public function error(\Exception $e)
    {
        $msg = sprintf('500 Internal Server Error %s (%s) %s',
            $e->getMessage(),
            $e->getCode(),
            $e->getTraceAsString()
        );
        try
        {
            $this->response(false)
                 ->status(500)
                 ->write($msg)
                 ->send();
        }
        catch (\Exception $ex)
        {
            exit($msg);
        }
    }

    public function notFound()
    {
        $this->response(false)
             ->status(404)
             ->write('404 Not Found')
             ->send();
    }
    
    public function json($data, $code = 200)
    {
        $this->response(false)
             ->status($code)
             ->header('Content-Type', 'application/json')
             ->write(json_encode($data))
             ->send();
    }

    public function route($pattern, $callback, $pass_route = false)
    {
        $this->router()->map($pattern, $callback, $pass_route);
    }
    
    public function path($dir)
    {
        $this->loader->addDirectory($dir);
    }
}
